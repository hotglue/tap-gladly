"""Stream type classes for tap-gladly."""

from __future__ import annotations

import csv
import json
from datetime import datetime, timedelta
from io import StringIO
from typing import Any, Iterable, Optional, Dict

import requests
from singer_sdk import typing as th
from singer_sdk.helpers.jsonpath import extract_jsonpath

from tap_gladly.client import gladlyStream
import os

class ConversationReportStream(gladlyStream):
    """Define custom stream."""

    name = "conversation_reports"
    path = "reports"
    primary_keys = ["Conversation ID"]
    replication_key = "Created At"
    rest_method = "POST"
    schema = th.PropertiesList(
        th.Property("Timezone Filter", th.StringType),
        th.Property("Inboxes Filter", th.StringType),
        th.Property("Created At", th.DateTimeType),
        th.Property("Conversation ID", th.StringType),
        th.Property("Customer ID", th.StringType),
        th.Property("Conversation Link", th.StringType),
        th.Property("Status", th.StringType),
        th.Property("Assigned Agent ID - Current", th.StringType),
        th.Property("Assigned Agent Name - Current", th.StringType),
        th.Property("Inbox ID - Current", th.StringType),
        th.Property("Inbox Name - Current", th.StringType),
        th.Property("First Closed At", th.DateTimeType),
        th.Property("Last Closed At", th.DateTimeType),
        th.Property("Initiator Type - Created", th.StringType),
        th.Property("Initiator Agent ID - Created", th.DateTimeType),
        th.Property("Initiator Agent Name - Created", th.DateTimeType),
        th.Property("Inbox ID - Created", th.DateTimeType),
        th.Property("Inbox Name - Created", th.DateTimeType),
        th.Property("Assigned Agent ID - Created", th.DateTimeType),
        th.Property("Assigned Agent Name - Created", th.DateTimeType),
        th.Property("Initiator Type - Last Closed", th.StringType),
        th.Property("Initiator Agent ID - Last Closed", th.StringType),
        th.Property("Initiator Agent Name - Last Closed", th.DateTimeType),
        th.Property("Initiator Rule ID - Last Closed", th.DateTimeType),
        th.Property("Inbox ID - Last Closed", th.DateTimeType),
        th.Property("Inbox Name - Last Closed", th.DateTimeType),
        th.Property("Assigned Agent ID - Last Closed", th.DateTimeType),
        th.Property("Assigned Agent Name - Last Closed", th.DateTimeType),
        th.Property("Topics", th.CustomType({"type": ["array", "string"]})),
        th.Property(
            "Topics with Hierarchy", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("First Contact ID", th.StringType),
        th.Property("First Channel", th.StringType),
        th.Property("Second Channel", th.StringType),
        th.Property("Third Channel", th.StringType),
        th.Property("Last Channel", th.StringType),
        th.Property("Created-to-First Closed Time in seconds", th.StringType),
        th.Property("Created-to-Last Closed Time in seconds", th.StringType),
        th.Property("Conversation Handle Time in seconds", th.StringType),
        th.Property("Is FCR Eligible", th.CustomType({"type": ["boolean", "string"]})),
        th.Property("Is FCR Success", th.CustomType({"type": ["boolean", "string"]})),
        th.Property(
            "Merged Conversations", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("Count of Contacts", th.StringType),
        th.Property("Count of Channels", th.StringType),
        th.Property("Count of Notes", th.StringType),
        th.Property("Count of Agents Assigned", th.StringType),
        th.Property("Count of Inboxes Assigned", th.StringType),
        th.Property("Times Agent Unassigned by Rule", th.StringType),
        th.Property("Times Closed", th.StringType),
        th.Property("Times Reopened", th.StringType),
        th.Property("Times Topic Added", th.StringType),
        th.Property("Times Topic Removed", th.StringType),
        th.Property("Payment Requests - Completed", th.StringType),
        th.Property("Payment Requests - Declined", th.StringType),
        th.Property("Payment Amount - Completed USD", th.StringType),
        th.Property("Payment Amount - Declined USD", th.StringType),
    ).to_dict()

    def prepare_request_payload(
        self,
        context: dict | None,
        next_page_token: Any | None,
    ) -> dict | None:
        start_at = self.get_starting_time(context)
        start_at_formatted = start_at.strftime("%Y-%m-%d")
        # do 14 day intervals on the report
        end_at = (start_at + timedelta(days=14)).strftime("%Y-%m-%d")
        payload = {
            "metricSet": "ConversationExportReport",
            "startAt": start_at_formatted,
            "endAt": end_at,
            # "filters": {"status": "CLOSED"},
        }
        return payload
    
    def get_timestamp_for_files(self):
        return (
            datetime
                .now()
                .replace(microsecond=0)
                .isoformat()
                .replace("+00:00", "")
                .replace("-", "")
                .replace(":", "")
        )

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        
        """Return a context dictionary for child streams."""
        return {
            "conversation_id": record["Conversation ID"],
        }

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        if response.status_code not in [404]:
            #Download the file in chunks
            filename = f"./conversion_report-{self.get_timestamp_for_files()}.csv"
            if response.status_code == 200:
                with open(filename, "wb") as file:
                    for chunk in response.iter_content(chunk_size=8192):
                        file.write(chunk)
            else:
                raise("Failed to download the file.")
            #Once downloaded iterate through the file and return results to avoid overloading the memory
            if os.path.isfile(filename):
                with open(filename, encoding="ISO-8859-1") as data:
                    data_reader = csv.DictReader(data)
                    for row in data_reader:
                        yield from list(extract_jsonpath(self.records_jsonpath, input=row))
            


class ConversationItemsStream(gladlyStream):
    """Define custom stream."""

    name = "conversation_items"
    path = "conversations/{conversation_id}/items"
    parent_stream_type = ConversationReportStream
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("conversationId", th.StringType),
        th.Property("content", th.CustomType({"type": ["object", "string"]})),
        th.Property("customerId", th.StringType),
        th.Property(
            "initiator",
            th.ObjectType(
                th.Property("type", th.StringType),
                th.Property("id", th.StringType),
            ),
        ),
        th.Property("timestamp", th.DateTimeType),
    ).to_dict()


class TeamsStream(gladlyStream):
    """Define custom stream."""

    name = "teams"
    path = "teams"
    primary_keys = ["id"]
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("agentIds", th.ArrayType(th.StringType)),
        th.Property("timestamp", th.DateTimeType),
    ).to_dict()

class JobsStream(gladlyStream):
    """Define custom stream."""

    name = "jobs"
    path = "export/jobs?status=COMPLETED"
    primary_keys = ["id"]
    first_run = True
    replication_key = "updatedAt"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("status", th.StringType),
        th.Property("updatedAt", th.DateTimeType),
        th.Property("parameters", th.ObjectType(
            th.Property("type", th.StringType),
            th.Property("startAt", th.DateTimeType),
            th.Property("endAt", th.DateTimeType),
        )),
        th.Property("files", th.ArrayType(th.StringType)),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        start_time = self.get_starting_time(context)  
        return {
            "job_id": record["id"],
            "updated_at": record["updatedAt"],
            "start_time": start_time
        }
    
    def _sync_children(self, child_context: dict) -> None:
        for child_stream in self.child_streams:
            if child_stream.selected or child_stream.has_selected_descendents:
                config_jobs = self.config.get("job_ids_list")
                if config_jobs and self.first_run:
                    self.first_run = False
                    for job in config_jobs:
                        config_jobs_child_context = {
                            "job_id": job
                        }
                        child_stream.sync(context=config_jobs_child_context)
                elif not config_jobs:
                    if child_context is not None:
                        start_time = child_context["start_time"].replace(tzinfo=None)
                        updated_at = datetime.strptime(child_context["updated_at"], '%Y-%m-%dT%H:%M:%S.%fZ')
                        if start_time and updated_at > start_time:
                            child_stream.sync(context=child_context)
    
    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        if response.status_code not in [404]:
            yield from extract_jsonpath(self.records_jsonpath, input=response.json())

class FilesStream(gladlyStream):
    records_jsonpath = "$."
    
    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        if response.status_code not in [404]:
            resp = response.text
            for line in resp.split("\n"):
                if line != '':
                    yield json.loads(line)

class AgentsFileStream(FilesStream):
    """Define custom stream."""

    name = "agents_files"
    parent_stream_type = JobsStream
    path = "export/jobs/{job_id}/files/agents.jsonl"
    primary_keys = ["id"]
    records_jsonpath = "$."
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("emailAddress", th.DateTimeType),
        th.Property("job_id", th.StringType),
    ).to_dict()

class ConversationItemsFileStream(FilesStream):
    """Define custom stream."""

    name = "conversation_items_files"
    parent_stream_type = JobsStream
    path = "export/jobs/{job_id}/files/conversation_items.jsonl"
    primary_keys = ["id"]
    records_jsonpath = "$."
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("conversationId", th.StringType),
        th.Property("customerId", th.StringType),
        th.Property("timestamp", th.DateTimeType),
        th.Property("initiator", th.ObjectType(
            th.Property("id", th.StringType),
            th.Property("type", th.StringType), 
        )),
        th.Property("content", th.CustomType({"type": ["object", "string"]})),
        th.Property("job_id", th.StringType),
    ).to_dict()

class CustomersFileStream(FilesStream):
    """Define custom stream."""

    name = "customers_files"
    parent_stream_type = JobsStream
    path = "export/jobs/{job_id}/files/customers.jsonl"
    primary_keys = ["id"]
    records_jsonpath = "$."
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("address", th.StringType),
        th.Property("emailAddresses", th.ArrayType(th.StringType)),
        th.Property("phoneNumbers", th.ArrayType(th.StringType)),
        th.Property("externalCustomerId", th.StringType),
        th.Property("job_id", th.StringType),
    ).to_dict()

class TopicsFileStream(FilesStream):
    """Define custom stream."""

    name = "topics_files"
    parent_stream_type = JobsStream
    path = "export/jobs/{job_id}/files/topics.jsonl"
    primary_keys = ["id"]
    records_jsonpath = "$."
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("disabled", th.BooleanType),
        th.Property("job_id", th.StringType),
    ).to_dict()
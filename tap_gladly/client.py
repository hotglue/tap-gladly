"""REST client handling, including gladlyStream base class."""

from __future__ import annotations

from pathlib import Path
from typing import Callable, Iterable

import requests
from pendulum import parse
from singer_sdk.authenticators import BasicAuthenticator
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream


class gladlyStream(RESTStream):
    """gladly stream class."""

    @property
    def url_base(self) -> str:
        """Return the API URL root, configurable via tap settings."""
        organization_url = self.config["organization_url"]
        is_sandbox = self.config.get("is_sandbox")
        domain_end = "qa" if is_sandbox else "com"
        url_base = f"https://{organization_url}.gladly.{domain_end}/api/v1/"
        return url_base

    records_jsonpath = "$[*]"

    @property
    def authenticator(self) -> BasicAuthenticator:
        """Return a new authenticator object.

        Returns:
            An authenticator instance.
        """
        return BasicAuthenticator.create_for_stream(
            self,
            username=self.config.get("username"),
            password=self.config.get("password"),
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed.

        Returns:
            A dictionary of HTTP headers.
        """
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    def validate_response(self, response: requests.Response) -> None:
        if (
            response.status_code in self.extra_retry_statuses
            or 500 <= response.status_code < 600
        ):
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)
        elif 400 <= response.status_code < 500 and response.status_code not in [404]:
            msg = self.response_error_message(response)
            raise FatalAPIError(msg)

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        if response.status_code not in [404]:
            yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    def post_process(self, row: dict, context=None):
        if len(row.keys()):
            return row
"""gladly tap class."""

from __future__ import annotations

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_gladly.streams import (
    ConversationItemsStream,
    ConversationReportStream,
    TeamsStream,
    JobsStream,
    AgentsFileStream,
    ConversationItemsFileStream,
    CustomersFileStream,
    TopicsFileStream
)

STREAM_TYPES = [ConversationItemsStream, 
                ConversationReportStream, 
                TeamsStream, 
                JobsStream,
                AgentsFileStream,
                ConversationItemsFileStream,
                CustomersFileStream,
                TopicsFileStream]


class Tapgladly(Tap):
    """gladly tap class."""

    name = "tap-gladly"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "username",
            th.StringType,
            required=True,
        ),
        th.Property(
            "password",
            th.StringType,
            required=True,
        ),
        th.Property(
            "organization_url",
            th.StringType,
            required=True,
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    Tapgladly.cli()
